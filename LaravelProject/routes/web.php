<?php

use App\Http\Controllers\PermissionsController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/



Route::post('/test', function (){ return 0; });

Route::get('/', function () {
    return view('welcome');
});

//ruta para establecer el token 
Route::get('/startSession', function (Request $req) {
    return  response()->json($req->header());
});

Route::post('/startSession', [PermissionsController::class, 'getViews']);


