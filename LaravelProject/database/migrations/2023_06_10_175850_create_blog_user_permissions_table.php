<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('TypePermissions', function (Blueprint $table) {
            $table->unsignedInteger('userType');
            $table->foreign('userType')->references('code')->on('user_type')->onDelete('cascade');;
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    { 
            Schema::dropIfExists('TypePermissions');
    }
};
