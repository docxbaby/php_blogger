<?php
require('db_connection.php');

$result = $worldConn->query('SELECT * FROM country');

if ($result) {

    $data = array();

    while ($row = mysqli_fetch_assoc($result)) {
        
        $data[$row['Code']]=$row['Name'];
    }

    echo json_encode( $data );

}

?>