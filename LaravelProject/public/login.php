<?php

include('db_connection.php'); 

// Handle the root ("/") route
if ($_SERVER['REQUEST_URI'] === '/login.php') {
    header('Content-Type: application/json');
    header('status: 200');

    $data = json_decode(file_get_contents('php://input'), true);
    $user = $data['user'];
    $pass = $data['pass'];
    $country = $data['country'];

    $hashedPassword = password_hash($pass, PASSWORD_DEFAULT);
    
    $sql = "SELECT * FROM users WHERE username = '" 
    . $user ."';"; 

    try {
        $result = $conn->query($sql);
        $num = mysqli_num_rows($result);

        if ( $num === 0 ){
            $data = array(
                'message' => 'UserNotFound',
            );

            echo json_encode($data); 
        }
        
        if ($result) {

            while ($row = mysqli_fetch_assoc($result)) {
                $rpass = $row['password']  ;

                $verified = password_verify($pass, $rpass);

                if ($verified) {
                    $data = array(
                        'message' => "Password is verified.",
                    );
                    echo json_encode($data);
                } else {
                    $data = array(
                        'message' => 'UserNotFound',
                    );
        
                    echo json_encode($data);
                }
            }
            
            // Free the result set
            mysqli_free_result($result);
        }

        if ($result === false) {
            $resp = [
                'Login' => false, 
            ];
            throw new Exception("Error executing the query: " . $conn->error);
        }
    
    } catch (Exception $e) {
        echo "Error al ejecutar la consulta: " . $e->getMessage();
    }
    
}
?>