<?php
namespace App\Http\Controllers;

use App\Models\BlogUser;
use App\Models\Permissions;
use Illuminate\Http\Request;

class UserController extends Controller
{

    public function searchUser(Request $request)
    {
        $user = new BlogUser();
        $userfound = $user->searchUser($request->user);
        $verified = false;  
     
        if (count( $userfound ) > 0 ){
            $verified = password_verify($request->password, $userfound[0]->password);
            if ($verified){
                return response()->json( $userfound[0] ) ;
            }
        }
        
        return response()->json( "User Not Found" ) ;
    }

    public function getViews(Request $req){
         
    }

} 