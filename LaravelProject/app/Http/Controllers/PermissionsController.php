<?php

namespace App\Http\Controllers;

use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\DB;


use Illuminate\Http\Request;

class PermissionsController extends Controller
{


    public function __construct(UserController $userCon)
    {
    }

    private function searchUser($req)
    {   
        $uc = new UserController(); 
        $user = $uc->searchUser($req);
        return $user;

    }

    public function getViews(Request $req)
    {
        $user = $this->searchUser($req);
        if ( $user->original === "User Not Found" )
            return  response()->json($user->original);

            $user = $user->original->username;
            $result = DB::table('blog_users AS u')
            ->join('typepermissions AS p','u.type', '=', 'p.userType')
            ->where('u.username', '=', $user)
            ->get();
            
            $result = json_decode($result);
            $result = $result[0]->description;

            return response()->json($result);
    }
}