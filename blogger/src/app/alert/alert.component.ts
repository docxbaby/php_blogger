import { Component, Input} from '@angular/core';
import { fadeIn } from 'src/animations';


@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.css'],
  animations: [
    fadeIn
  ]
})
export class AlertComponent {
  @Input() verified: boolean = false;
  @Input() showAlert: boolean = false;
}
