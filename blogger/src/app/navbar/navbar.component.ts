import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProfileService } from '../profile.service';


@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})

export class NavbarComponent implements OnInit {

  isLogged = false;

  constructor(private router: Router, private profileService: ProfileService) {
    let profile = profileService.getUserProfile();
    console.log(!!profile);
  }

  ngOnInit(): void {
    this.profileService.getLoginEmitter().subscribe((logged:boolean)=>{
      this.isLogged = logged;
    });
  }

  goToLogin(event: Event) {
    event.preventDefault();
    this.router.navigate(['/login']);

  }

  goToHome(event: Event) {
    event.preventDefault();
    this.router.navigate(['/home']);

  }

  goToProfile(event: Event) {
    event.preventDefault();
    this.router.navigate(['/sign']);

  }

  goToSignin(event: Event) {
    event.preventDefault();
    this.router.navigate(['/sign']);

  }



}
