import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { fadeIn } from 'src/animations';
import { Router } from '@angular/router';
import { ProfileService } from '../profile.service';



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  animations: [
    fadeIn
  ]
})

export class LoginComponent implements OnInit {

  user: string = "";
  pass: string = "";
  country: string = "";
  userVerified: boolean = false;
  showAlertBox: boolean = false;
  eyeHover: boolean = false;
  showPassword: boolean = true;

  constructor(private http: HttpClient, private router: Router, private profileService: ProfileService) {

  }

  token: string = "";

  ngOnInit(): void {
  }

  onHover() {
    this.eyeHover = true;
  }

  onHoverEnd() {
    this.eyeHover = false;

  }

  reveal() {
    this.showPassword = !this.showPassword;
  }

  loading: boolean = false;

  getXSRF_Cookie(): string {
    let cookies = document.cookie.split(';');
    for (let i = 0; i < cookies.length; i++) {
      if (cookies[i].trim().startsWith('XSRF-TOKEN')) {
        this.token = cookies[i].trim();
        return decodeURIComponent(this.token)
      }
    }
    return ''
  }

  bloggerView = 'main blogger view'

  sendLogin(event: Event) {
    this.http.post('http://localhost/startSession', { user: this.user, password: this.pass })
      .subscribe({
        next: (res) => {

          let userData = Object.entries(res);
          let userType = userData[9][1];

          this.profileService.setUserProfile(res);
          
          if (userType === 1) {
            this.router.navigate(['/blogger']);
          }

          if (userType === 0) {
            this.router.navigate(['/admin']);
          }
        }
        ,
        error: (res) => { console.log("Error", res) }
      });
  }
}
