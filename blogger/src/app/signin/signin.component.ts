import { Component, NgIterable, OnInit } from '@angular/core';
import { fadeIn } from 'src/animations';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css'],
  animations: [fadeIn]
})

export class SigninComponent implements OnInit {

  user: string = "";
  showPassword: boolean = true;
  pass: string = "";
  eyeHover: boolean = false;
  countryCodes: any[] = [];
  showConf: boolean = true;
  selectedCode: string = "";

  revealConf() {
    this.showConf = !this.showConf;
  }

  reveal() {
    this.showPassword = !this.showPassword;
    console.log("revela", this.showPassword);
  }

  onHover() {
    this.eyeHover = !this.eyeHover;

  }

  constructor(private http: HttpClient) {

  }

  codes: NgIterable<any> = []

  ngOnInit(): void {
    this.http.get('http://localhost/world.php').subscribe({
      next: (res) => {
        let values = Object.values(res);
        this.codes = values;
      },
      error: (res) => {
        console.log("Error no se han obtenido los paises", res)
      }
    })
  }

  userField: string = '';
  isInvalidUser: boolean = false;

  passField: string = "";
  isInvalidPass: boolean = false;

  //pass confirmation
  conf: string = "";
  isInvalidConf: boolean = false;
  userAvailable: string = ""
  usrTooShort: boolean = false;

  sendUser(event: Event) {
    this.isInvalidUser = this.userField.length == 0;
    this.isInvalidPass = this.passField.length == 0;
    this.isInvalidConf = this.conf.length == 0;

    //si algun dato falta = true 
    let missinData = (this.isInvalidUser || this.isInvalidPass || this.isInvalidConf);

    const MininumCharLength = 3;

    //si no falta info y la longitud del usuario es mayor a 4
    if (!missinData && this.userField.length > MininumCharLength) {
      this.http.post('http://localhost/getUser.php', { "user": this.userField }).subscribe({
        next: (res) => {
          this.userAvailable = res.toString();
          let canInsert = this.userAvailable === "Usable" ? true : false;

          //cumple con los requisitos, insertamos
          if (canInsert) { this.tryInsertUser() } },
              error: (res) => { console.log("error buscando usuario", res)   }
      })
    } else {
      this.usrTooShort = true;
    }
  }

  tryInsertUser() {
    this.http.post("http://localhost/newUser.php",
      {
        'user': this.userField,
        'password': this.passField,
        "code": this.selectedCode
      }).subscribe({
        next:(res)=>{console.log(res)},
        error:(res)=>{
          res = 
          console.log("no se pudo", res['error'])
        }
      });
  }

}
