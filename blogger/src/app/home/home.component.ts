import { Component } from '@angular/core';
import { fadeIn } from 'src/animations';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  animations: [fadeIn]
})
export class HomeComponent {

  constructor(private routes: Router) {

  }

  goToSignin(event: Event) {
    this.routes.navigate(['/sign'])
  }
}
