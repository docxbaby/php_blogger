import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';


@Injectable({
  providedIn: 'root'
})

export class ProfileService {
  userProfile:any; 
  private loginEmitter: Subject<boolean> = new Subject<boolean>();


  isLogged(){
  }

  constructor( ) { 
  }

  setUserProfile(profile:any){
    console.log(profile)
    this.userProfile = profile;
    this.loginEmitter.next(true);

  }

  getLoginEmitter() {
    return this.loginEmitter.asObservable();
  }

  getUserProfile(){
    return this.userProfile;
  }
}
