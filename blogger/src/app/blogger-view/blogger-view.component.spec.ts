import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BloggerViewComponent } from './blogger-view.component';

describe('BloggerViewComponent', () => {
  let component: BloggerViewComponent;
  let fixture: ComponentFixture<BloggerViewComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [BloggerViewComponent]
    });
    fixture = TestBed.createComponent(BloggerViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
